# cdn

*Third-party libraries*

[![Codacy Badge](https://api.codacy.com/project/badge/Grade/af68cd814a414fffb2596b2979aeba8b)](https://www.codacy.com/app/englishextra/cdn?utm_source=github.com&utm_medium=referral&utm_content=englishextra/cdn&utm_campaign=badger)
[![Travis](https://img.shields.io/travis/englishextra/cdn.svg)](https://github.com/englishextra/cdn)

## Usage

> Public repositories on GitHub are often used to share open source software. Open source software is software that is licensed so that others are free to use, change, and distribute the software.

<https://help.github.com/articles/open-source-licensing/>

## Default Push URL

```
https://github.com/englishextra/cdn.git
```

## Remotes

* [GitHub](https://github.com/englishextra/cdn)
* [BitBucket](https://bitbucket.org/englishextra/cdn)
* [GitLab](https://gitlab.com/englishextra/cdn)

## Copyright

© [github.com/englishextra](https://github.com/englishextra), 2015-2018

